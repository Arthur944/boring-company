import { config, server } from '@litbase/server';
import { AddressInfo } from 'net';
import { registerJoins } from './database';
import { setupListeners } from './listeners';

config.redisUrl = 'http://localhost:6379' as string;
config.sessionSecret = 'super-secret-secret-please-dont-guess-it';
config.providers = {
  email: {
    fromAddress: 'server@boring-company.com' as string,
  },
};

config.mongoOptions = {
  address: 'mongodb://localhost:27017',
  database: 'boring-company-db',
};

config.port = 8080;
config.address = 'localhost';
config.useMongoWatchStream = false;
config.userQueryFields = {
  _id: 1,
  name: 1,
  isAdmin: 1,
};

(async function () {
  try {
    await server.startListening();
    registerJoins();
    setupListeners();
    console.debug('Litbase server is listening on ', (server.fastify.server.address() as AddressInfo)?.port);
  } catch (error) {
    console.error(error);
  }
})();
