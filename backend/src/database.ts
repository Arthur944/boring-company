import { litql } from '@litbase/core';

export function registerJoins() {
  litql.registerJoin(
    {
      type: 'users',
      joinField: '_id',
      joinType: 'many',
      field: 'simulations',
    },
    {
      type: 'simulations',
      joinField: 'userId',
      joinType: 'one',
      field: 'user',
    }
  );

  litql.registerJoin(
    {
      type: 'users',
      joinField: 'profilePictureId',
      joinType: 'one',
      field: 'profilePicture',
    },
    {
      type: 'profile-pictures',
      joinField: '_id',
      joinType: 'many',
      field: 'users',
    }
  );

  litql.registerJoin(
    {
      type: 'labels',
      joinField: '_id',
      joinType: 'many',
      field: 'simulations',
    },
    {
      type: 'simulations',
      joinField: 'labelIds',
      joinType: 'many',
      field: 'labels',
    }
  );
}
