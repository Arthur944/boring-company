import { litql } from '@litbase/core';

export function setupListeners() {
  litql.on('beforeInsert', onlyAllowAdminToEditProtectedCollections);
  litql.on('beforeUpdate', onlyAllowAdminToEditProtectedCollections);
  litql.on('beforeRemove', onlyAllowAdminToEditProtectedCollections);
  litql.on('beforeUpsert', onlyAllowAdminToEditProtectedCollections);
}

function onlyAllowAdminToEditProtectedCollections(event: any) {
  if (['labels', 'profile-pictures'].includes(event.resolution.collection.name)) {
    if (!event.resolution.context.request.user.isAdmin) {
      event.cancel = true;
    }
  }
}
