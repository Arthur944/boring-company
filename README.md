# BoringCompany

## 1. Követelmény analízis

#### Az oldal célja a Boring Company (https://www.boringcompany.com) által tervezettközelekedési alternatíva szimulációja.

A cég célja hogy egy metrókhoz hasonló alagút rendszert építsenek, amikben kisméretű elektromos autók viszik
az utasokat hosszú vonatszerelvények helyett. Ahoz hogy ez gazdaságilag működni tudjon le 
szeretnék csökkenteni az alagútfúrás költségét, és az autókat önvezető módon működtetni az alagutakban.

Egy nyitott kérdés hogy szabad piaci körülmények között lehetséges lenne-e egy ilyen rendszert működtetni,
és ha igen akkor mennyire olcsóvá kellene tenni az alagútfúrást, valamint a szerelvények beszerzését/fenttartsát,
valamint kérdések merülnek fel a rendszer kapacitásával kapcsolatban is.
Alternatívát jelenthet egy ilyen megoldás a metróhálózatokhoz?

Ezeknek a kérdések megválaszolásában hivatott segítséget nyújtani az oldal, a rendszer virtuális
szimulációjával.

## 1.1 Funkcionális elvárások

 - Regisztárció / Bejelentkezés
 - Profil oldal, ahol megváltoztatható a felhasználónév és profilkép
 - Admin felhasználó számára legyen lehetőség profil képek és címkék feltöltésére, szerkesztésére listázására és törlésére.
 - Szimulációk listázása
 - Új szimuláció létrehozásának lehetősége
 - A szimulációban szerelvények hozzáadásának lehetősége
 - A szimulációban a szerelvények folyamatosan mozogjanak az elérhető állomások között, és szimulálják az utas felvételt/leadást véletlenszerű hosszúságú várakozásokkal az állomásokon. A szerelvények legyenek tekintettel egymásra, és lassítsanak le akkor ha ennek hiányában összeütköznének.
 - Szimuláció elmenthető legyen. Ekkor a szimulációnak lehessen megadni nevet, és választani az elérhető címkék közül.
 - Már elmentett szimulációt később lehessen megnyitni, és tovább futtatni

## 1.2 Nem funkcionális elvárások

- Felhasználóbarát, egyszerű, letisztult felület
- Jelszavas azonosítás, jelszavak biztonságos tárolása
- Admin funkciók letiltása biztonságos módon nem admin felhasználóknak

## 1.3.1 Use-case modell
![img_1.png](img_1.png)

## 1.3.2 Szimuláció kreálás menete
![img_2.png](img_2.png)

# 2. Tervezés

# 2.1 Oldaltérkép
- Bejelentkezés oldal
- Regisztráció oldal
- Szimulációkat listázó oldal
  - Új szimuláció hozzáadása
  - Meglévő szimuláció szerkesztése
  - Kijelentkezés
- Egy megnyitott szimuláció oldala
  - Szimuláció elmentése
  - Visszalépés a lista oldalra
- Profil beállításokat szerkesztő oldal
  - Felhasználónév és profilkép szerkesztése
  - Admin felhasználóknak:
    - Címkék és profilkép lehetőségek kezelése

# 2.2 Végpontok
Az oldal a litbase keretrendszert használja, amivel nem szükséges külön end-pointok léterhozása
adatok lekérdezésére, módosítására, létrehozására és törlésére. Ezek a funkciók
mind elérhetőek a `litql.query` függvénnyel.

 - `litql.query("users", {$matchAll: {}})` - felhasználók listázása
 - `litql.query("users", {$match: {_id: USER_ID }, $pull: 1})` - felhasználó törlése
 - `litql.query("users", {$match: {_id: USER_ID }, $upsert: {$set: USER_DATA }})` - felhasználó létrehozása/szerkesztése
 - `litql.query("simulations", {$matchAll: {}})` - szimulációk listázása
 - `litql.query("simulations", {$match: {_id: SIMULATION_ID }, $pull: 1})` - szimuláció törlése
 - `litql.query("simulations", {$match: {_id: SIMULATION_ID }, $upsert: {$set: SIMULATION_DATA }})` - szimuláció létrehozása/szerkesztése
 - `litql.query("profile-pictures", {$matchAll: {}})` - profilképek listázása
 - `litql.query("labels", {$matchAll: {}})` - címkék listázása
 - Az alábbi funkciók csak admin felhasználók számára elérhetől:
   - `litql.query("profile-pictures", {$match: {_id: PROFILE_PICTURE_ID }, $pull: 1})` - profilkép törlése
   - `litql.query("profile-pictures", {$match: {_id: PROFILE_PICTURE_ID }, $upsert: {$set: PROFILE_PICTURE_DATA }})` - profilkép létrehozása/szerkesztése
   - `litql.query("labels", {$match: {_id: LABEL_ID }, $pull: 1})` - címkék törlése
   - `litql.query("labels", {$match: {_id: LABEL_ID }, $upsert: {$set: LABEL_DATA }})` - címkék létrehozása/szerkesztése

# 2.3 Design

## Bejelentkezés oldal
![img_6.png](img_6.png)

## Regisztrációs oldal
![img_7.png](img_7.png)

## Szimulációkat listázó oldal
![img_3.png](img_3.png)

## Egy megnyitott szimuláció oldala
![img_4.png](img_4.png)

## Profil oldal
![img_5.png](img_5.png)


## 2.4 Adatmodell
![img_8.png](img_8.png)

# 3. Fejlesztőkörnyezet (backend)

## Felhasznált eszközök
- Git verziókezelő
- Node.js Javascript környezet
- Litbase keretrendszer
- MongoDB adatbázis
- Gitlab a projekt közzétételéhez
- Yarn 2 csomagkezelő
- Redis autentikációban való segítséghez

## Fejlesztőkörnyezet felállítása

1. Git verziókzelő telepítése
2. Node.js (v16+) telepítése
3. Yarn telepítése
4. MongoDB telepítése (vagy távoli adatbázis link beszerzése)
5. Redis telepítése (vagy távoli instancia link beszerzése)
6. Projekt klónozása lokális gépre
7. Függöségek telepítése (`yarn install`)
8. App indítása: `yarn start`

## Könyvtárstruktúra
- `/` - A root mappában található package.json és .yarnrc.yml tartalmazzák a dependenciák beszerzéséhez szükséges konfigurációkat.
- `.yarn`
  - `releases` - Itt található a használt yarn verzió. Használatéhoz szükséges a yarn alapjának telepítése
  - `plugins` - Itt találhatók a plugin-ok, amiket yarn-hoz használunk
- `src` - Alkalmazás logika
  - `index.ts` - A fájl amit a szerver elindításakor futtatunk
  - `database.ts` - Itt definiáljuk az adatbázis tábla kapcsolatokat
  - `listeners.ts` - Itt vannak definiálva a figyelők, amik a különböző táblákon való változtatásokkor nem engedélyezik a megfelelő jogosultságok nélküli hozzáférét.

# Fejlesztőkörnyezet (frontend)

## Program futtatása

1. Yarn telepítése
2. `yarn install`
3. Backend elindítása
4. `yarn start`

## Könyvtárstruktúra

- src
  - app - Itt találhatóak a komponenseink, guardjaink, serviceink, moduljaink
    - 
  - assets - Itt találhatóak a statikus asset-ek, amiket felhasználunk.
  - typings - Itt található a @litbase/core library által szolgáltatott UserInterface interfész felüldefiniálása az egyedi mezőinkkel

# Felhasználói dokumentáció
Az oldalon szimulációkat elérni/futtatni csak bejelentkezés után lehet

Regisztrációhoz kattints a "Register" gombra. Itt e-mail címed, felhasználóneved és jelszavad segítségével regisztrálhatsz.

Bejelentkezéshez írd be a felhasználóneved és jelszavad.

## Szimulációk böngészése

Bejelentkezés után a szimulációk listája fogad. Nyisd meg egy korábbi szimulációdat, vagy hozz létre egy újat az "új létrehozása" gombra kattintva
Régi szimulációdat törölheted a kuka ikonra kattintva.

## Szimuláció kezelése

A szimulációban hozzá tudsz adni autókat az "Add car" gombra kattintva. Elmentheted a szimulációt a bal felső sarokban található
mentés ikonra kattintva. Ekkor egy felület jelenik meg ahol elnevezheted a szimulációt, és kiválaszthatsz hozzá címkéket.
A vissza nyílra kattintva vissza tudsz navigálni a lista oldalra

## Profil oldal
A lista oldalon bal alsó sarokban található "Profile" gombra kattintva a profil oldaladra kerülsz. Itt megváltoztathatod a felhasználóneved,
valamint a profilképed.

## Adminisztrátor lehetőségei

Admin felhasználóként a profil oldalon beállíthatod az elérhető profilképeket, valamint a címkéket.
