import { client, config } from '@litbase/client';

export function startLitbaseClient() {
  config.url = getWebsocketUrl('http://localhost:8080');
  config.debug = true;
  config.currentUserFields = {
    _id: 1,
    name: 1,
    providers: 1,
    profilePictureId: 1,
    isAdmin: 1,
    profilePicture: {
      _id: 1,
      name: 1,
      svg: 1,
    },
  };
  client.connect();
}

function getWebsocketUrl(httpUrl: string) {
  const url = new URL(httpUrl);

  if (url.protocol === 'http:') {
    url.protocol = 'ws:';
  } else {
    url.protocol = 'wss:';
  }

  url.pathname = '/ws';
  return url.toString();
}
