import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { CarComponent } from './components/simulation-components/car/car.component';
import { SceneComponent } from './components/simulation-components/scene/scene.component';
import { NgxThreeModule } from 'ngx-three';
import { OrbitControlsComponent } from './components/simulation-components/orbit-controls/orbit-controls.component';
import { SceneBackgroundComponent } from './components/simulation-components/scene-background/scene-background.component';
import { AppPath } from './components/simulation-components/path/path.component';
import { StationComponent } from './components/simulation-components/station/station.component';
import { SimulationPageComponent } from './components/pages/simulation-page/simulation-page.component';
import { LoginPageComponent } from './components/pages/login-page/login-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterPageComponent } from './components/pages/register-page/register-page.component';
import { SimulationListPageComponent } from './components/pages/simulation-list-page/simulation-list-page.component';
import { SimulationListElemComponent } from './components/simulation-list-elem/simulation-list-elem.component';
import { ProfilePageComponent } from './components/pages/profile-page/profile-page.component';

@NgModule({
  declarations: [
    AppComponent,
    CarComponent,
    SceneComponent,
    OrbitControlsComponent,
    SceneBackgroundComponent,
    AppPath,
    StationComponent,
    SimulationPageComponent,
    LoginPageComponent,
    RegisterPageComponent,
    SimulationListPageComponent,
    SimulationListElemComponent,
    ProfilePageComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, NgxThreeModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
