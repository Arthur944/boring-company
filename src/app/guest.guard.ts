import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { filter, firstValueFrom, map, Observable } from 'rxjs';
import { client } from '@litbase/client';
import { isEmptyUser, isUserGuest } from '@litbase/core';

@Injectable({
  providedIn: 'root',
})
export class GuestGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return firstValueFrom(
      client.currentUser$.pipe(
        filter((user) => !isEmptyUser(user)),
        map((user) => {
          const allowed = isUserGuest(user);
          if (!allowed) {
            return this.router.parseUrl('/home');
          }
          return allowed;
        })
      )
    );
  }
}
