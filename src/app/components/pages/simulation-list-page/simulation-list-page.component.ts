import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Simulation } from '../../../types';
import { litql } from '@litbase/core';
import { client } from '@litbase/client';
import { Router } from '@angular/router';

@Component({
  selector: 'app-simulation-list-page',
  templateUrl: './simulation-list-page.component.html',
})
export class SimulationListPageComponent implements OnInit {
  public isLoading = true;
  public simulations: Simulation[] = [];

  constructor(private cdr: ChangeDetectorRef, private router: Router) {}

  ngOnInit(): void {
    litql
      .query<Simulation>('simulations', {
        $matchAll: {},
        $live: true,
        _id: 1,
        name: 1,
        user: { _id: 1, name: 1 },
        labels: { _id: 1, name: 1 },
      })
      .subscribe((simulations) => {
        this.simulations = simulations;
        this.isLoading = false;
        this.cdr.detectChanges();
      });
  }

  public async onLogout() {
    await client.logout();
    this.router.navigate(['/login']);
  }

  @ViewChild('resultCard')
  set resultCard(_input: ElementRef | undefined) {
    if (_input !== undefined) {
      // autoAnimate(_input.nativeElement);
    }
  }
}
