import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { client } from '@litbase/client';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
})
export class LoginPageComponent {
  loginForm = this.formBuilder.group({
    email: '',
    password: '',
  });

  constructor(private formBuilder: FormBuilder, private router: Router) {}

  async onSubmit(): Promise<void> {
    await client.loginWithEmailAndPassword({
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    });
    setTimeout(() => {
      this.router.navigate(['/home']);
    }, 250);
  }
}
