import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { randInt } from 'three/src/math/MathUtils';
import { Label, PathPoint, Simulation } from '../../../types';
import { Car, cars } from '../../../cars';
import { StationsService } from '../../../services/stations.service';
import { CarComponent } from '../../simulation-components/car/car.component';
import { ActivatedRoute } from '@angular/router';
import { BinaryId, createBinaryId, litql, UserInterface } from '@litbase/core';
import autoAnimate from '@formkit/auto-animate';
import { FormBuilder } from '@angular/forms';
import { client } from '@litbase/client';

@Component({
  selector: 'app-root',
  templateUrl: './simulation-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimulationPageComponent implements OnInit {
  title = 'boring-company';
  public rotation: [x: number, y: number, z: number] = [0, 0, 0];
  public stations: PathPoint[] = [];
  public cars: Simulation['cars'] = [];
  public carComponents: CarComponent[] = [];
  public isLoading = true;
  public isSaveOpen = false;
  public isAddingLabels = false;
  public _id?: BinaryId;

  public labels: Label[] = [];
  simulationForm = this.formBuilder.group({
    name: '',
    labelIds: [],
  });

  constructor(
    private stationsService: StationsService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef
  ) {
    stationsService.stations$.subscribe((stations) => (this.stations = stations));
  }

  public ngOnInit() {
    this.route.params.subscribe(async (params) => {
      if (!params['id'] || params['id'] === 'new') {
        this.isLoading = false;
        this._id = createBinaryId();
      } else {
        this._id = BinaryId.fromUrlSafeString(params['id']);
        const simulation = await litql.query<Simulation>('simulations', {
          $match: { _id: this._id },
        });
        if (!simulation) return;
        this.simulationForm.get('name')?.setValue(simulation.name);
        this.simulationForm.get('labelIds')?.setValue(simulation.labelIds?.map((elem) => elem.toUrlSafeString()) || []);
        this.cars = simulation.cars;
        this.isLoading = false;
      }
      this.cdr.detectChanges();
    });
    litql.query<Label>('labels', { $matchAll: {}, $live: true }).subscribe((labels) => {
      this.labels = labels;
    });
  }

  public onBeforeRender() {
    this.rotation = [0, this.rotation[2] + 0.01, this.rotation[2] + 0.01];
  }

  public onChange(e: any, index: number, station: PathPoint) {
    const newStations: PathPoint[] = [...this.stationsService.getStations()];
    const newPosition = station.position;
    newPosition[index] = e.target.value;
    newStations[newStations.findIndex((elem) => elem.id === station.id)] = {
      ...station,
      position: newPosition,
    } as PathPoint;
    this.stationsService.updateStations(newStations);
  }

  public onAddStation() {
    this.stationsService.addNewStation();
  }

  public onAddCar() {
    this.cars.push({ id: this.cars.length + 1 });
  }

  public recieveCar(car: CarComponent) {
    if (!this.carComponents.some((elem) => elem.car.id === car.car.id)) {
      this.carComponents.push(car);
    }
  }

  async onSubmit() {
    await litql.query<Simulation>('simulations', {
      $match: { _id: this._id },
      $upsert: {
        $set: {
          name: this.simulationForm.value.name,
          userId: client.currentUser._id,
          labelIds: this.simulationForm.value.labelIds.map((elem: string) => BinaryId.fromUrlSafeString(elem)),
          cars: this.carComponents.map((elem) => ({
            id: elem.car.id,
            position: elem.position,
            destination: elem.destination,
            positionInfo: elem.positionInfo,
            speed: elem.speed,
          })),
          points: this.stationsService.points$.getValue(),
        },
      },
    });
    this.isSaveOpen = false;
  }

  @ViewChild('saveCard')
  set saveCard(_input: ElementRef | undefined) {
    if (_input !== undefined) {
      autoAnimate(_input.nativeElement);
    }
  }
}
