import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { client } from '@litbase/client';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
})
export class RegisterPageComponent {
  registerForm = this.formBuilder.group({
    email: '',
    password: '',
    name: '',
  });

  constructor(private formBuilder: FormBuilder, private router: Router) {}

  async onSubmit(): Promise<void> {
    await client.registerWithEmailAndPassword({
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
      name: this.registerForm.value.name,
    });
    setTimeout(() => {
      this.router.navigate(['/home']);
    }, 250);
  }
}
