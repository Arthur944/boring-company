import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { client } from '@litbase/client';
import { BinaryId, createBinaryId, litql } from '@litbase/core';
import { Label, ProfilePicture } from '../../../types';
import { DomSanitizer } from '@angular/platform-browser';
import autoAnimate from '@formkit/auto-animate';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
})
export class ProfilePageComponent implements OnInit {
  public profilePictures: ProfilePicture[] = [];
  public loadingPics = true;
  public labels: Label[] = [];
  public loadingLabels = true;
  public isAddingProfilePicture = false;
  public isAddingLabel = false;

  profileForm = this.formBuilder.group({
    name: '',
    profilePictureId: '',
  });

  profilePictureForm = this.formBuilder.group({
    _id: createBinaryId(),
    profilePictureName: '',
    svg: '',
  });

  labelForm = this.formBuilder.group({
    _id: createBinaryId(),
    labelName: '',
  });

  constructor(private formBuilder: FormBuilder, private cdr: ChangeDetectorRef, private sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    this.profileForm.get('name')?.setValue(client.currentUser.name);
    this.profileForm.get('profilePictureId')?.setValue(client.currentUser.profilePictureId?.toUrlSafeString());
    litql.query<ProfilePicture>('profile-pictures', { $matchAll: {}, $live: true }).subscribe((profilePictures) => {
      this.profilePictures = profilePictures;
      this.loadingPics = false;
      this.cdr.detectChanges();
    });
    litql.query<Label>('labels', { $matchAll: {}, $live: true }).subscribe((labels) => {
      this.labels = labels;
      this.loadingLabels = false;
      this.cdr.detectChanges();
    });
  }

  public startEditingProfilePicture(picture = { _id: createBinaryId(), name: '', svg: '' }) {
    this.profilePictureForm.get('_id')?.setValue(picture._id);
    this.profilePictureForm.get('profilePictureName')?.setValue(picture.name);
    this.profilePictureForm.get('svg')?.setValue(picture.svg);
    this.isAddingProfilePicture = true;
    this.cdr.detectChanges();
  }

  public startEditingLabel(label = { _id: createBinaryId(), name: '' }) {
    this.labelForm.get('_id')?.setValue(label._id);
    this.labelForm.get('labelName')?.setValue(label.name);
    this.isAddingLabel = true;
    this.cdr.detectChanges();
  }

  async submitProfilePictureForm() {
    const { _id, ...values } = this.profilePictureForm.value;
    await litql.query('profile-pictures', {
      $match: { _id: _id },
      $upsert: {
        $set: {
          name: values.profilePictureName,
          svg: values.svg,
        },
      },
    });
    this.isAddingProfilePicture = false;
    this.cdr.detectChanges();
  }

  async submitLabelForm() {
    const { _id, ...values } = this.labelForm.value;
    await litql.query('labels', {
      $match: { _id: _id },
      $upsert: {
        $set: {
          name: values.labelName,
        },
      },
    });
    this.isAddingLabel = false;
    this.cdr.detectChanges();
  }

  getSvgHtml(svg: string) {
    return this.sanitizer.bypassSecurityTrustHtml(svg);
  }

  async deletePicture(picture: ProfilePicture) {
    await litql.query('profile-pictures', { $match: { _id: picture._id }, $pull: 1 });
    this.cdr.detectChanges();
  }

  async deleteLabel(label: Label) {
    await litql.query('labels', { $match: { _id: label._id }, $pull: 1 });
    this.cdr.detectChanges();
  }

  async onProfileFormSubmit() {
    await litql.query('users', {
      $match: { _id: client.currentUser._id },
      $set: {
        name: this.profileForm.value.name,
        profilePictureId: BinaryId.fromUrlSafeString(this.profileForm.value.profilePictureId),
      },
    });
    this.cdr.detectChanges();
  }

  cancelProfilePictureEdit() {
    this.isAddingProfilePicture = false;
    this.cdr.detectChanges();
  }

  cancelLabelEdit() {
    this.isAddingLabel = false;
    this.cdr.detectChanges();
  }

  getDisplayableProfilePictures() {
    return this.profilePictures.filter(
      (elem) => !this.isAddingProfilePicture || !elem._id.equals(this.profilePictureForm.value._id)
    );
  }

  getDisplayableLabels() {
    return this.labels.filter((elem) => !this.isAddingLabel || !elem._id.equals(this.labelForm.value._id));
  }

  @ViewChild('pictureList')
  set pictureList(_input: ElementRef | undefined) {
    if (_input !== undefined) {
      autoAnimate(_input.nativeElement);
    }
  }

  @ViewChild('pictureCard')
  set pictureCard(_input: ElementRef | undefined) {
    if (_input !== undefined) {
      autoAnimate(_input.nativeElement);
    }
  }

  @ViewChild('labelList')
  set labelList(_input: ElementRef | undefined) {
    if (_input !== undefined) {
      autoAnimate(_input.nativeElement);
    }
  }

  @ViewChild('labelCard')
  set labelCard(_input: ElementRef | undefined) {
    if (_input !== undefined) {
      autoAnimate(_input.nativeElement);
    }
  }

  public isAdmin() {
    return true || !!client.currentUser.isAdmin;
  }
}
