import {Component, Input, OnInit} from "@angular/core";
import {Scene} from "three";
import {ThEngineService, ThPerspectiveCamera} from "ngx-three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";

@Component({
  selector: 'orbit-controls',
  template: ``,
})
export class OrbitControlsComponent implements OnInit {

  @Input() scene?: Scene;
  @Input() camera?: ThPerspectiveCamera<any, any>;
  engineService: ThEngineService;

  constructor(thEngineService: ThEngineService) {
    this.engineService = thEngineService;
  }

  ngOnInit(): void {

    console.log({engine: this.engineService, renderer: this.engineService.renderer, camera: this.camera});
  }

}
