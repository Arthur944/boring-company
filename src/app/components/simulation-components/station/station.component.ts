import { Component, Input, OnInit } from '@angular/core';
import { BufferGeometry, Curve, DoubleSide, EllipseCurve } from 'three';
import { Font, FontLoader } from 'three/examples/jsm/loaders/FontLoader';
import helvetiker from 'three/examples/fonts/helvetiker_bold.typeface.json';

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
})
export class StationComponent implements OnInit {
  @Input() position: [number, number] = [0, 0];
  @Input() id: number = -1;
  public curve: BufferGeometry;
  public doubleSide = DoubleSide;
  public font: Font;
  constructor() {
    const curve = new EllipseCurve(
      0,
      0, // ax, aY
      1,
      1, // xRadius, yRadius
      0,
      2 * Math.PI, // aStartAngle, aEndAngle
      false, // aClockwise
      0 // aRotation
    );
    this.curve = new BufferGeometry().setFromPoints(curve.getPoints(50));
    const fontLoader = new FontLoader();
    this.font = fontLoader.parse(helvetiker);
  }

  ngOnInit(): void {}
}
