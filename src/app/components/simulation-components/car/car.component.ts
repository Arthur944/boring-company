import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Car, metersToUnits, unitsToMeters } from '../../../cars';
import { BufferGeometry, Clock, Curve, DoubleSide, Scene, Vector3 } from 'three';
import { PathPoint, Simulation } from '../../../types';
import { PathSegment, PathService } from '../../../services/path.service';
import { distance, StationsService } from '../../../services/stations.service';
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry';
import { randInt } from 'three/src/math/MathUtils';
import { sum } from 'lodash-es';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
})
export class CarComponent implements OnInit {
  @Input() car!: Simulation['cars'][number];
  @Input() scene?: Scene;
  @Input() destination!: PathPoint;
  @Input() cars: CarComponent[] = [];
  @Output() selfEmitter = new EventEmitter<CarComponent>();
  private pathCurve?: Curve<any>;
  public pathGeometry: BufferGeometry;
  public speed = metersToUnits(0);
  private pathProgress: number | null = null;
  position: [number, number, number] = [0, 0, 0];
  rotation: [number, number, number] = [0, 0, 0];
  private prevDate = new Date();
  private clock = new Clock();
  public positionInfo: { segmentStart: number; segmentEnd: number; startReachedAt: number } = {
    segmentStart: 1,
    segmentEnd: 1,
    startReachedAt: 0,
  };
  private waitTime = randInt(2, 5);
  private topSpeed = metersToUnits(50);
  topDeltaV = 10;
  private d = 0;
  private wholePath?: PathSegment[];
  public estimatedPositions?: [number, number][];

  constructor(private pathService: PathService, private stationsService: StationsService) {
    this.pathGeometry = new LineGeometry();
  }

  ngOnInit() {
    this.position = this.car.position || this.position;
    this.positionInfo = this.car.positionInfo || this.positionInfo;
    this.destination = this.car.destination || this.destination;
    this.speed = this.car.speed || this.speed;
    if (!this.car.destination) {
      this.onPathSegmentEndReached();
    } else {
      const { curve, wholePath } = this.pathService.getPath(this, this.destination?.id);
      this.pathCurve = curve;
      this.wholePath = wholePath;
    }
    this.selfEmitter.emit(this);
  }

  public onBeforeRender() {
    this.prevDate = new Date();
    this.estimatedPositions = undefined;
    if (this.pathCurve) {
      this.followPath(this.pathCurve);
    } else {
      this.onPathSegmentEndReached();
    }
  }

  private followPath(path: Curve<any>) {
    this.d = this.clock.getDelta();
    this.pathProgress = this.updatePathProgress();
    this.modulateSpeed();
    if (this.pathProgress >= path.getLength()) {
      this.onPathSegmentEndReached();
    } else {
      const [x, z] = path.getPointAt(this.pathProgress / path.getLength()).toArray();
      this.position = [x, 0, z];
      const tangent = path.getTangentAt(this.pathProgress / path.getLength());
      const angle = Math.atan2(tangent.x, tangent.y) - Math.PI * 0.5;
      const finalAngle = (angle < 0 ? Math.PI * 2 + angle : angle) + Math.PI / 2;
      this.rotation = [0, finalAngle, 0];
    }
  }

  private modulateSpeed() {
    for (const other of this.getNearbyCars()) {
      if (this.shouldDecelerateForCar(other)) {
        this.decelerate();
        return;
      }
    }
    if (this.positionInfo.segmentStart === this.destination.id) {
      this.speed = 0;
      return;
    }
    const distanceLeft = this.getDistanceUntilDestionation();
    const timeToStop = this.speed / this.topDeltaV;
    if (timeToStop * (this.speed / 2) >= distanceLeft) {
      this.decelerate();
      return;
    }
    if (this.speed < this.topSpeed) {
      this.speed += this.d * this.topDeltaV;
      this.speed = Math.min(this.speed, this.topSpeed);
    }
  }

  private decelerate() {
    this.speed -= this.d * this.topDeltaV;
    this.speed = Math.max(this.speed, 0.1);
    return;
  }

  private getDistanceUntilDestionation() {
    if (!this.wholePath) return 0;
    const segments = this.wholePath.slice(
      this.wholePath.findIndex((elem) => elem.start.id === this.positionInfo.segmentStart),
      this.wholePath.findIndex((elem) => elem.end.id === this.destination.id) + 1
    );
    if (!segments[0]) return 0;
    const currentSegmentDistance = segments[0].curve.getLength() - (this.pathProgress || 0);
    const distances = segments.slice(1).map((elem) => elem.curve.getLength());
    return sum([currentSegmentDistance, ...distances]);
  }

  private updatePathProgress() {
    this.pathProgress = (this.pathProgress || 0) + this.d * this.speed;
    return this.pathProgress;
  }

  public getPrevReachedPoint() {
    return this.positionInfo.segmentStart;
  }

  private onPathSegmentEndReached() {
    // If we just reached the end of a segment, we're not currently between two points, we're exactly on one point
    if (!this.destination || this.positionInfo.segmentEnd !== this.destination?.id) {
      this.positionInfo.segmentStart = this.positionInfo.segmentEnd;
      this.positionInfo.startReachedAt = this.clock.getElapsedTime();
      this.pathProgress = 0;
      if (this.destination) {
        const { curve, start, end, wholePath } = this.pathService.getPath(this, this.destination?.id);
        this.positionInfo.segmentEnd = end.id;
        this.positionInfo.segmentStart = start.id;
        this.pathCurve = curve;
        this.wholePath = wholePath;
        this.pathGeometry = new BufferGeometry().setFromPoints(
          this.pathCurve.getPoints(50).map((elem) => new Vector3(elem.x, 0, elem.y))
        );
      }
    } else {
      if (this.clock.getElapsedTime() - this.positionInfo.startReachedAt > this.waitTime) {
        this.destination = this.pathService.getNextDestination(this.destination);
        this.waitTime = randInt(2, 5);
      }
    }
  }

  public getPathProgressPercent() {
    return (this.pathProgress || 0) / (this.pathCurve?.getLength() || 1);
  }

  private getNearbyCars(): CarComponent[] {
    return this.cars.filter((elem) => elem.car.id !== this.car.id && distance(elem.position, this.position) < 150);
  }

  public isOnMainPath() {
    return !this.stationsService
      .getStations()
      .some((elem) => [this.positionInfo.segmentStart, this.positionInfo.segmentEnd].includes(elem.id));
  }

  private shouldDecelerateForCar(other: CarComponent) {
    const maxDistance = 15;

    if (!this.estimatedPositions) {
      this.estimatedPositions = this.getEstimatedFuturePositions();
    }
    if (!other.estimatedPositions) {
      other.estimatedPositions = other.getEstimatedFuturePositions();
    }
    const willCollide = this.estimatedPositions.find(
      (pos, index) => other.estimatedPositions?.[index] && distance(other.estimatedPositions[index], pos) < maxDistance
    );
    if (!willCollide) {
      return false;
    }
    if (this.positionInfo.segmentEnd === other.positionInfo.segmentStart) {
      return true;
    }
    if (this.positionInfo.segmentStart === other.positionInfo.segmentEnd) {
      return false;
    }
    return this.getPathProgressPercent() < other.getPathProgressPercent();
  }

  public getEstimatedFuturePositions() {
    const seconds = 5;
    const divisions = 50;
    const estimations = [];
    for (let i = 0; i < seconds; i += seconds / divisions) {
      const distance = i * this.speed;
      const estimatedPosition = getPositionInDistance(
        distance,
        this.wholePath || [],
        this.getPathProgressPercent() || 0
      );
      if (!estimatedPosition) return estimations;
      estimations.push(estimatedPosition);
    }
    return estimations;
  }
}

function getPositionInDistance(
  distance: number,
  wholePath: PathSegment[],
  currentPercentage: number
): [number, number] | null {
  if (!wholePath.length) return null;
  const leftFromCurrentSegment = wholePath[0].curve.getLength() * (1 - currentPercentage);
  if (leftFromCurrentSegment > distance) {
    return wholePath[0].curve.getPointAt(currentPercentage + distance / wholePath[0].curve.getLength()).toArray();
  }
  let remainingDistance = distance - leftFromCurrentSegment;
  for (const elem of wholePath.slice(1)) {
    if (remainingDistance < elem.curve.getLength()) {
      return elem.curve.getPointAt(remainingDistance / elem.curve.getLength()).toArray();
    } else {
      remainingDistance -= elem.curve.getLength();
    }
  }
  return null;
}
