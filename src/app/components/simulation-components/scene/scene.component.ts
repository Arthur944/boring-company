import { Component, OnInit } from '@angular/core';
import { cars } from '../../../cars';
import { ThCamera, ThObject3D, ThScene } from 'ngx-three';
import { Camera, Scene, WebGLRenderer } from 'three';

@Component({
  selector: 'app-scene',
  templateUrl: './scene.component.html',
})
export class SceneComponent extends ThScene {
  public lookAtVector: [number, number, number] = [15, 0, 0];

  constructor(parent: ThObject3D) {
    super(parent);
  }

  override ngOnInit() {
    super.ngOnInit();
  }

  public getScene(): Scene | undefined {
    return this._objRef;
  }
}
