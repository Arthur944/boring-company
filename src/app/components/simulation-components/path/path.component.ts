import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit } from '@angular/core';
import { BufferGeometry, Curve, DoubleSide, EllipseCurve, Path, SplineCurve, Vector2 } from 'three';
import { metersToUnits } from '../../../cars';
import { PathPoint } from '../../../types';
import { getSmoothPathBetweenPoints } from '../../../services/path.service';
import { StationsService } from '../../../services/stations.service';

@Component({
  selector: 'app-path',
  templateUrl: `./path.component.html`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppPath implements OnInit, OnChanges {
  @Input() width: number = metersToUnits(20);
  @Input() length: number = metersToUnits(300);
  doubleSide = DoubleSide;
  ellipseGeometry: BufferGeometry;
  public curve: Curve<any>;
  public position: [number, number, number] = [0, 0, 0];
  direction = 1;
  private stationService: StationsService;
  public stations: PathPoint[];

  constructor(stationService: StationsService) {
    this.curve = new Path();
    this.ellipseGeometry = new BufferGeometry().setFromPoints(this.curve.getPoints(50));
    this.stationService = stationService;
    this.stations = this.stationService.getStations();
    this.stationService.stations$.subscribe((stations) => {
      this.stations = stations;
      this.renderPath();
    });
  }

  public ngOnInit() {
    this.renderPath();
  }

  public onBeforeRender() {}

  public ngOnChanges() {
    this.renderPath();
  }

  private renderPath() {
    const pathPoints = this.stationService.points$.getValue();
    this.curve = getSmoothPathBetweenPoints(
      this.stationService.connectedStations.map(([fromId, toId]) => [
        new Vector2().fromArray(pathPoints.find((elem) => elem.id === fromId)!.position),
        new Vector2().fromArray(pathPoints.find((elem) => elem.id === toId)!.position),
      ])
    );

    const points = this.curve.getPoints(50);
    this.ellipseGeometry = new BufferGeometry().setFromPoints(points);
  }
}
