import { Component, OnInit } from '@angular/core';
import {ThEngineService} from "ngx-three";
import {Color} from "three";

@Component({
  selector: 'scene-background',
  template: '',
})
export class SceneBackgroundComponent implements OnInit {
  engineService: ThEngineService;

  constructor(engineService: ThEngineService) {
    this.engineService = engineService;
  }

  ngOnInit(): void {
    this.engineService.renderer?.setClearColor(new Color("gray"));
  }

}
