import { ChangeDetectionStrategy, Component } from '@angular/core';
import autoAnimate from '@formkit/auto-animate';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['../app.styles.css'],
})
export class AppComponent {
  constructor() {}

  ngOnInit(): void {}
}
