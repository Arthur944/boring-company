import { Component, Input, OnInit } from '@angular/core';
import { Simulation } from '../../types';
import { client } from '@litbase/client';
import { litql } from '@litbase/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-simulation-list-elem',
  templateUrl: './simulation-list-elem.component.html',
})
export class SimulationListElemComponent implements OnInit {
  @Input() public simulation!: Simulation;
  public isOwn = false;

  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    this.isOwn = client.currentUser._id.equals(this.simulation?.userId);
  }

  public onDelete() {
    litql.query('simulations', { $match: { _id: this.simulation._id }, $pull: 1 });
  }

  public getProfilePictureSvg() {
    return this.sanitizer.bypassSecurityTrustHtml(client.currentUser.profilePicture?.svg || '');
  }
}
