import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SimulationPageComponent } from './components/pages/simulation-page/simulation-page.component';
import { LoginPageComponent } from './components/pages/login-page/login-page.component';
import { RegisterPageComponent } from './components/pages/register-page/register-page.component';
import { SimulationListPageComponent } from './components/pages/simulation-list-page/simulation-list-page.component';
import { AuthGuard } from './auth.guard';
import { GuestGuard } from './guest.guard';
import { ProfilePageComponent } from './components/pages/profile-page/profile-page.component';

const routes: Routes = [
  { path: 'simulation', component: SimulationPageComponent, canActivate: [AuthGuard] },
  { path: 'home', component: SimulationListPageComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfilePageComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginPageComponent, canActivate: [GuestGuard] },
  { path: 'register', component: RegisterPageComponent, canActivate: [GuestGuard] },
  { path: '**', redirectTo: '/login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
