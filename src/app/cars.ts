export interface Car {
  id: number;
}

export const cars: Car[] = [{ id: 1 }, {id: 2}];

export function metersToUnits(meters: number) {
  return meters;
}

export function unitsToMeters(units: number) {
  return units;
}
