import { Injectable } from '@angular/core';
import { PathPoint } from '../types';
import { BehaviorSubject, map } from 'rxjs';
import { Vector2 } from 'three';

@Injectable({
  providedIn: 'root',
})
export class StationsService {
  public points$;
  public stations$;
  public connectedStations = [
    [1, 2],
    [2, 3],
    [3, 4],
    [4, 5],
    [5, 6],
    [6, 0],
  ];

  constructor() {
    const points = this.calculatePoints([
      { id: 1, isStation: true, position: [0, 0] },
      { id: 2, isStation: true, position: [0, 200] },
      { id: 3, isStation: true, position: [0, 400] },
      { id: 4, isStation: true, position: [100, 400] },
      { id: 5, isStation: true, position: [100, 200] },
      { id: 6, isStation: true, position: [100, 0] },
    ]);
    this.points$ = new BehaviorSubject<PathPoint[]>(points);
    this.recalculateConnections(points);
    this.stations$ = this.points$.pipe(map((points) => points.filter((elem) => elem.isStation)));
  }

  public updateStations(stations: PathPoint[]) {
    const points = this.calculatePoints(stations);
    this.recalculateConnections(points);
    this.points$.next(points);
  }

  private recalculateConnections(points: PathPoint[]) {
    const connections = [];
    const orderedPoints = [...this.getRightStations(points), ...this.getLeftStations(points)]
      .map((station) => [
        points.find((elem) => elem.id === station.id + 1),
        station,
        points.find((elem) => elem.id === station.id - 1),
      ])
      .flat()
      .reverse() as PathPoint[];
    for (let i = 0; i < orderedPoints.length; i += 3) {
      connections.push(
        [orderedPoints[i].id, orderedPoints[i + 1].id],
        [orderedPoints[i].id, orderedPoints[i + 2].id],
        [orderedPoints[i + 1].id, orderedPoints[i + 2].id],
        [orderedPoints[i + 2].id, orderedPoints[i + 3]?.id || orderedPoints[0].id]
      );
    }
    this.connectedStations = connections;
  }

  public addNewStation() {
    const leftStations = this.getLeftStations();
    const rightStations = this.getRightStations();

    let newPosition: [number, number];
    if (rightStations.length > leftStations.length) {
      newPosition = [100, Math.max(...leftStations.map((elem) => elem.position[1])) + 200];
    } else {
      newPosition = [0, Math.max(...rightStations.map((elem) => elem.position[1])) + 200];
    }
    this.updateStations([
      ...this.getStations(),
      { id: this.getStations().length + 1, position: newPosition, isStation: true },
    ]);
  }

  private getLeftStations(inputStations?: PathPoint[]) {
    const stations = inputStations || this.getStations();
    return stations
      .filter((elem) => elem.position[0] !== 0 && elem.isStation)
      .sort((a, b) => a.position[1] - b.position[1]);
  }

  private getRightStations(inputStations?: PathPoint[]) {
    const stations = inputStations || this.getStations();
    return stations
      .filter((elem) => elem.position[0] === 0 && elem.isStation)
      .sort((a, b) => b.position[1] - a.position[1]);
  }

  public getStations() {
    return this.points$.getValue().filter((elem) => elem.isStation);
  }

  private calculatePoints(stations: PathPoint[]) {
    return stations
      .map((station) => {
        const position = [Number(station.position[0]), Number(station.position[1])];
        const isRight = position[0] == 0;
        const branchOffPoint = [position[0] + (isRight ? -1 : 1) * 50, position[1] + (isRight ? -1 : 1) * 50];
        const connectBackPoint = [position[0] + (isRight ? -1 : 1) * 50, position[1] + (isRight ? 1 : -1) * 50];
        return [
          { id: 0, position: branchOffPoint },
          { id: 0, position: position, isStation: true },
          { id: 0, position: connectBackPoint },
        ];
      })
      .flat()
      .map((elem, index) => ({ ...elem, id: index + 1 })) as PathPoint[];
  }
}

export function distance(
  vec1: [number, number] | [number, number, number],
  vec2: [number, number] | [number, number, number]
) {
  return new Vector2()
    .fromArray([vec1[0], vec1.slice(-1)[0]])
    .distanceTo(new Vector2().fromArray([vec2[0], vec2.slice(-1)[0]]));
}
