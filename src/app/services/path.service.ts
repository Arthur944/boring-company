import { Injectable } from '@angular/core';
import { Curve, Path, Vector2 } from 'three';
import { PathPoint } from '../types';
import { aStar } from 'ngraph.path';
import createGraph, { Graph } from 'ngraph.graph';
import { CarComponent } from '../components/simulation-components/car/car.component';
import { sample } from 'lodash-es';
import { StationsService } from './stations.service';
import { startWith } from 'rxjs';

export interface PathSegment {
  curve: Curve<any>;
  start: PathPoint;
  end: PathPoint;
}

@Injectable({
  providedIn: 'root',
})
export class PathService {
  private pathPoints: PathPoint[];
  private paths: Curve<any>[];
  private graph: Graph<number>;
  private stationMap: Map<number, PathPoint>;

  constructor(private stationsService: StationsService) {
    this.pathPoints = [];
    this.paths = [];
    this.graph = createGraph();
    this.stationMap = new Map();
    stationsService.points$.pipe(startWith(stationsService.points$.getValue())).subscribe((stations) => {
      this.pathPoints = stations;
      this.paths = this.stationsService.connectedStations.map(([station1, station2]) => {
        const path = new Path();
        path.moveTo(...this.getPathPoint(station1).position);
        path.lineTo(...this.getPathPoint(station2).position);
        return path;
      });
      this.graph = createGraph<number>();
      for (const [id1, id2] of this.stationsService.connectedStations) {
        this.graph.addLink(id1, id2);
      }
      this.stationMap = new Map(this.pathPoints.map((elem) => [elem.id, elem]));
    });
  }

  public getPath(car: CarComponent, destinationId: number): PathSegment & { wholePath: PathSegment[] } {
    const pathFinder = aStar<number, number>(this.graph, { oriented: true });
    const path = pathFinder.find(car.getPrevReachedPoint(), destinationId);
    const pathSegments = path
      .map((elem, index) =>
        !path[index + 1]
          ? null
          : this.getPathSegment(
              this.stationMap.get(path[index + 1].id as number)!,
              this.stationMap.get(elem.id as number)!
            )
      )
      .filter((elem) => !!elem)
      .reverse() as PathSegment[];
    return {
      ...pathSegments[0],
      wholePath: pathSegments,
    };
  }

  private getPathSegment(point1: PathPoint, point2: PathPoint) {
    return {
      curve: getSmoothPathBetweenPoints([
        [new Vector2().fromArray(point1.position), new Vector2().fromArray(point2.position)],
      ]),
      start: point1,
      end: point2,
    };
  }

  public getStations() {
    return this.pathPoints;
  }

  public getNextDestination(from: PathPoint): PathPoint {
    return sample(this.pathPoints.filter((elem) => elem.id !== from.id && elem.isStation)) as PathPoint;
  }

  private getPathPoint(id: number) {
    return this.pathPoints.find((elem) => elem.id === id) as PathPoint;
  }
}

export function getSmoothPathBetweenPoints(points: [Vector2, Vector2][]): Curve<any> {
  const path = new Path();
  for (const point of points) {
    path.moveTo(...point[0].toArray());
    path.lineTo(...point[1].toArray());
  }
  return path;
}
