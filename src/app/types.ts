import { BinaryId, UserInterface } from '@litbase/core';
import { CarComponent } from './components/simulation-components/car/car.component';

export interface PathPoint {
  position: [number, number];
  id: number;
  isStation: boolean;
}

export interface Simulation {
  _id: BinaryId;
  name: string;
  userId: BinaryId;
  user?: UserInterface;
  cars: {
    id: number;
    position?: [number, number, number];
    destination?: PathPoint;
    positionInfo?: CarComponent['positionInfo'];
    speed?: number;
  }[];
  points: PathPoint[];
  labelIds?: BinaryId[];
  labels?: Label[];
}

export interface ProfilePicture {
  _id: BinaryId;
  name: string;
  svg: string;
}

export interface Label {
  _id: BinaryId;
  name: string;
}
