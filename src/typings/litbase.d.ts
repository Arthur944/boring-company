import type { UserInterface as OriginalUserInterface } from '@litbase/core';
import { ProfilePicture } from '../app/types';
import { BinaryId } from '@litbase/core';

declare module '@litbase/core' {
  // @ts-ignore
  export interface UserInterface extends OriginalUserInterface {
    profilePictureId?: BinaryId;
    profilePicture?: ProfilePicture;
    isAdmin?: boolean;
  }
}
