import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { startLitbaseClient } from "./litbase-client";

if (environment.production) {
  enableProdMode();
}

startLitbaseClient();

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
